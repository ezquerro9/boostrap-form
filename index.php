<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
         <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>David Form</title>
    </head>
    <body>
        <form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>DOGS</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nombre">Name</label>  
  <div class="col-md-4">
  <input id="nombre" name="nombre" type="text" placeholder="" class="form-control input-md">
  <span class="help-block">Entry the dog name </span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="raza">Race</label>  
  <div class="col-md-4">
  <input id="raza" name="raza" type="text" placeholder="" class="form-control input-md">
  <span class="help-block">Entry the dog race</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="peso">Weight</label>  
  <div class="col-md-4">
  <input id="peso" name="peso" type="number" placeholder="" class="form-control input-md">
  <span class="help-block">Entry the dog weight</span>  
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Edad">Age</label>
  <div class="col-md-4">
    <select id="Edad" name="Edad" class="form-control">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
      <option value="13">13</option>
      <option value="14">14</option>
      <option value="15">15</option>
    </select>
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Genero">Female/Male</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="Genero-0">
      <input type="radio" name="Genero" id="Genero-0" value="1" checked="checked">
      Female
    </label>
	</div>
  <div class="radio">
    <label for="Genero-1">
      <input type="radio" name="Genero" id="Genero-1" value="2">
      Male
    </label>
	</div>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="aceptar/cancelar"></label>
  <div class="col-md-8">
    <button id="aceptar/cancelar" name="aceptar/cancelar" class="btn btn-success">Accept</button>
    <button id="button2id" name="button2id" class="btn btn-danger">Cancel</button>
  </div>
</div>

</fieldset>
</form>

    </body>
</html>
